const { request } = ("express");
const req = require("express/lib/request");
const { leerInfo, guardarInfo } = require("../models/guardarInfo");
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';
//en el db mandar una copia de la lista
const personas = new Personas(); //lista


const tareasDB = leerInfo()
if(tareasDB){
  //establecer las tareas
  personas.cargarTareasFormArray(tareasDB);
  // console.log('DDDB', tareasDB, 'DBBBd');
  personas._listado = tareasDB
  // console.log('22', personas._listado, '2222');
}

const personaGet = (req, res = response) => {

  const lista = leerInfo();
  // console.log(lista, 'hahah');
  res.json({
    msg: 'post API - Controldor',
    lista,
  });
};

const personaPut =  (req = request, res = response) => {
  const  {id} = req.params;
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const index = personas._listado.findIndex(object => object.id === id)

  if (index !== -1){
    personas.editarPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: 'persona editada',
    })
  } else {
    res.json ({
      msg: 'person no encontrada',
    })
  }

};

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona( nombres, apellidos, ci, direccion, sexo )
  console.log('3333', persona, '333');

//crear el metodo crearpersona
  personas.crearPersona(persona)
  console.log('000',personas._listado, '000');
  guardarInfo(personas._listado)
  const lista = personas._listado
  
  res.json({
    msg: 'persona Creada',
    // lista,
  });

  // personas._listado.push(lista)
  
};

const personaDelete = (req, res = response) => {
  // const  {id} = req.params;
  
  // const index = personas._listado.findIndex(object => object.id === id) 

  // if (index !== -1){
  //   personas.eliminarPersona(index)
  //   guardarInfo(personas._listado)
  //   res.json({
  //     msg: 'person eliminada - Controlador',
  //   })
  // } else {
  //   res.json ({
  //     msg: 'person no encontrada',
  //   })
  // }


  const { id } = req.params
  // console.log('idd', id, 'idd');
  // console.log('eliminar');
  if (id) {
    personas.eliminarPersona(id);
    guardarInfo(personas.personasArr)

    res.json({
    msg: 'persona eliminada - Controlador',
    })
    // console.log(leerDB());
  }

};

const personaGetNA = (req = request, res = response) =>{
  const {nombres, apellidos} = req.query;
  console.log(req.query);

  const index = personas._listado.findIndex(object => object.nombres === nombres && object.apellidos === apellidos)
  // console.log(index);

  const dato = personas._listado[index]
 console.log(dato);

  if (index !== -1){
      res.json({
        msg: 'persona encontrada',
        dato
      })
    } else {
      res.json ({
        msg: 'persona no encontrada',
      })
    }
}

const personaGetCi = (req = request, res = response) =>{
  const {ci} = req.query;
  console.log(parseInt(ci));
  const cid = parseInt(ci)
  const index = personas._listado.findIndex(object => object.ci === cid) 
  console.log(index);

  const dato = personas._listado[index]
  console.log(dato);
  if (index !== -1){
    res.json({
      msg: 'persona encontrada',
      dato
    })
  } else {
    res.json ({
      msg: 'persona no encontrada',
    })
  }
}

const personaGetSexo = (req = request, res = response) =>{
  const {sexo} = req.query;
  //aplicar for each o algo asi
  const index = personas._listado.findIndex(object => object.sexo === sexo) 
  console.log(index);

  const dato = personas._listado[index]
  console.log(dato);

  if (index !== -1){
    res.json({
      dato
    })
  } else {
    res.json ({
      msg: 'persona no encontrada',
    })
  }
}
const personaGetObtGen =(req = request, res = response
  ) => {
    const {obt, genero} = req.params;
    console.log(obt);
    console.log(personas._listado);
   
    // const index = personas._listado.findIndex(object => object.apellidos.startsWith(obt) || object.genero === genero)

    // console.log(index);
    
    // const dato = personas._listado[index]
    // console.log(dato.apellidos.startsWith(obt));
    // if (index !== -1){
    //   res.json({
    //     msg: 'persona encontrada',
    //     dato
    //   })
    // } else {
    //   res.json ({
    //     msg: 'persona no encontrada',
    //   })
    // }
}
  
module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete,
  personaGetNA, 
  personaGetCi,
  personaGetSexo,
  personaGetObtGen
}
