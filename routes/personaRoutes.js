const { Router } = require('express');
const { personaGet, personaPut, personaPost, personaDelete, personaGetNA, personaGetCi, personaGetSexo,  personaGetObtGen } = require('../controllers/personaController');
// console.log('personaRoutes');

const router = Router();

router.get('/', personaGet);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);

router.get('/buscar', personaGetNA);

router.get('/CI', personaGetCi);

router.get('/sexo', personaGetSexo);

router.get('/:obt/:genero',personaGetObtGen)


module.exports = router;